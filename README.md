**Contributors:**

Piotr Gorzelnik

Patryk Szydlik

**Project:**

Squick Micromouse

**Brief:**

Repository contains datasheets and documentation related to parts and elements used in our project


**Repository Content:** 

 -- 	BT  (Bluetooth module)
 
 --     Czujnik_IR  (IR sensor)
 
 -- 	IMU  (Inertial Measurement Unite)
 
 -- 	Napęd  (Motors, motor drivers and encoders)
 
 -- 	VL  (VL laser time of flight sensors)
 
 -- 	Zasilanie  (Power supply)
 
 
 

